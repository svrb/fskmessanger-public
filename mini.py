#!python3

from numpy import int16, zeros, append, int32
import random
import string
from os import remove, mkdir, sep
import gnupg  # python-gnupg
from subprocess import run, PIPE
from scipy.io.wavfile import read
from scipy.io.wavfile import write
import toml
from platform import uname
import io
import zipfile
from PyQt6 import uic, QtWidgets, QtGui
from PyQt6.QtCore import pyqtRemoveInputHook
from PyQt6.QtWidgets import QFileDialog
getOpenFileName, getSaveFileName = QFileDialog.getOpenFileName, QFileDialog.getSaveFileName


class gui():
    def __init__(self, gpg):
        self.gpg = gpg
        self.recive_method = ''
        self.save_video_cover = False
        self.ui = uic.loadUi('mini.ui')
        self.ui.save_raw.clicked.connect(self.save_raw)
        self.ui.save_mp3.clicked.connect(self.save_mp3)
        self.ui.save_video.clicked.connect(self.save_video)
        self.ui.recive_do.clicked.connect(self.recive_do)
        self.ui.video_cover.clicked.connect(self.video_cover)
        self.ui.input_raw.clicked.connect(self.input_raw)
        self.ui.input_file.clicked.connect(self.input_file)
        # self.ui.input_link.clicked.connect(self.input_link)
        self.ui.clear.clicked.connect(self.ui.recive_msg.clear)
        self.ui.save.clicked.connect(self.save)
        self.ui.load.clicked.connect(self.load)
        self.ui.gpg_ui.clicked.connect(gpg_ui.ui.show)
        self.ui.dui.clicked.connect(dui.ui.show)
        self.ui.importKey.clicked.connect(self.importKey)
        self.ui.exportKey.clicked.connect(self.exportKey)
        self.ui.symmetric.toggled.connect(self.changeCryptoMode)
        self.ui.asymmetric.toggled.connect(self.changeCryptoMode)

        self.cryptoMode = 'symmetric'

        if uname().system == 'Linux':
            self.minimodem = 'minimodem'
            self.ffmpeg = 'ffmpeg'
            self.temp = 'temp/'

        else:  # on windows
            self.minimodem = 'minimodem.exe'
            self.ffmpeg = 'ffmpeg.exe'
            self.temp = 'temp\\'

        if len(self.temp) != 0:
            try:
                mkdir(self.temp)
            except FileExistsError:
                pass

    def save(self):
        settings = {
            'symmetric': self.ui.symmetric.isChecked(),
            'asymmetric': self.ui.asymmetric.isChecked(),
            'metadata': self.ui.metadata.isChecked(),
            'metadata_size': self.ui.metadata_size.value(),
            'sps': self.ui.sps.value(),
            'f0': self.ui.f0.value(),
            'f1': self.ui.f1.value(),
            'buad_rate': self.ui.buad_rate.value(),
            'sample_rate': self.ui.sample_rate.value(),
            'volume': self.ui.volume.value()
        }
        f_address, _ = getSaveFileName(
            None, "ذخیره تنظیمات", "", "conf file (*.conf)")
        if not _:
            return
        with open(f_address, 'w') as f:
            toml.dump(settings, f)
        self.ui.config_status.setText('تنظیمات ذخیره شد')

    def load(self):
        f_address, _ = getOpenFileName(
            None, "بارگذاری تنظیمات", "", "conf file (*.conf)")
        if not _:
            return
        with open(f_address) as f:
            settings = toml.load(f)
        self.ui.symmetric.setChecked(settings['symmetric']),
        self.ui.asymmetric.setChecked(settings['asymmetric']),
        self.ui.metadata.setChecked(settings['metadata']),
        self.ui.metadata_size.setValue(settings['metadata_size']),
        self.ui.sps.setValue(settings['sps']),
        self.ui.f0.setValue(settings['f0']),
        self.ui.f1.setValue(settings['f1']),
        self.ui.buad_rate.setValue(settings['buad_rate']),
        self.ui.sample_rate.setValue(settings['sample_rate']),
        self.ui.volume.setValue(settings['volume'])
        self.ui.config_status.setText('تنظیمات اعمال شد')

    def powerOfWind(self):
        fs, cover = read(self.temp+'cover.wav')
        cover = cover.copy()
        fs, msg_compact = read(self.temp+'mz.wav')
        sample_rate = self.ui.sample_rate.value()
        buad_rate = self.ui.buad_rate.value()
        sample_per_seg = self.ui.sps.value()
        if sample_per_seg == 0:
            sample_per_seg = sample_rate // buad_rate
        metadataSize = 0

        if self.ui.metadata.isChecked():
            metadataSize = self.ui.metadata_size.value()
            if msg_compact.size > 16777215:  # 2^24 only 3 byte reserve for msgsize in meta sapace
                self.ui.send_status.setText(
                    'خطا:حجم پیام از مقدار مجاز بیشتر است  یا پیام را کوتاه کنید یا متادیتا را غیر فعال کنید')
                return False
            msgsize_fix_width = f'{msg_compact.size:024b}'
            msgsize_fix_width = [msgsize_fix_width[i*8:i*8+8]
                                 for i in range(3)]  # split into 3*8 slice
            msgsize_fix_width = [int(i, 2)
                                 for i in msgsize_fix_width]  # convert to int
            with open(self.temp+'mzws', 'wb') as f:
                f.write(bytes(msgsize_fix_width))
            del (f)
            cmd = f'{
                self.minimodem} -t -f {self.temp}mzwsw.wav -R 44100 -M 500 -S 5000 2205'
            with open(self.temp+'mzws') as f:
                r = run(cmd, stdin=f, shell=True)
                if r.returncode != 0:
                    self.ui.send_status.setText('خطا در ساخت متادیتا')
                    return
            _, msw = read(self.temp+'mzwsw.wav')
            if msw.size >= metadataSize:
                self.ui.send_status.setText('خطا:اندازه متادیتا خیلی کم است')
                return False
            cover[cover.size - metadataSize: cover.size -
                  metadataSize + msw.size] = msw
            # print('metadat start from sample',cover.size - metadataSize)
            # end meta
        else:
            self.alert(
                'i', f' عدد {msg_compact.size} را یادداشت کنید و به گیرنده بدهید', save='send')
        # +1 is for make an extra segment(length=gap) for avoid overlap
        gap = (cover.size - metadataSize) // ((msg_compact.size//sample_per_seg)+1)
        # print('',,((msg_compact.size//sample_per_seg))

        if gap < sample_per_seg:
            self.ui.send_status.setText(
                'خطا:تراکم دانه ها زیاد است و باد قدرت پخش کردن شان را ندارد!')
            return False
        if gap < 1000:
            self.ui.send_status.setText('توجه: باد می وزد ولی ضعیف !')
        # means: available > needed
        if (cover.size % gap)+gap < msg_compact.size % sample_per_seg + metadataSize:
            self.alert('e', 'کار کردن با مقادیر جاری ناممکن است لطفا مقدار ابرها یا اندازه متادیتا را عوض کنید',
                       save='send')
            return False
        i = 0
        fixLastFramsize = 0
        # print(cover.size,msg_compact.size,gap,sample_per_seg)
        while i*sample_per_seg < msg_compact.size:
            if msg_compact.size - i*sample_per_seg < sample_per_seg:
                fixLastFramsize = sample_per_seg - \
                    (msg_compact.size - i*sample_per_seg)
                # print(i,i*gap,i*sample_per_seg,msg_compact.size,fixLastFramsize)
            # print(i, fixLastFramsize , (i*gap+sample_per_seg - fixLastFramsize)-i*gap,
                # msg_compact.size - i*sample_per_seg , gap,sample_per_seg,cover.size - i*gap,msg_compact.size)
            cover[i*gap:i*gap+sample_per_seg - fixLastFramsize] = msg_compact[i *
                                                                              sample_per_seg:(i+1)*sample_per_seg]
            i += 1
        write(self.temp+'mo.wav', sample_rate, cover)
        return True

    def dumper(self):
        f0 = self.ui.f0.value()
        f1 = self.ui.f1.value()
        buad_rate = self.ui.buad_rate.value()
        sample_rate = self.ui.sample_rate.value()
        sample_per_seg = self.ui.sps.value()
        if sample_per_seg == 0:
            sample_per_seg = sample_rate // buad_rate
        metadata_size = 0

        if f0 // buad_rate == f1 // buad_rate:
            self.alert(
                'e', 'با توجه به سرعت مقدار f0 و f1 خیلی به هم نزدیک اند. f0یاf1یا سرعت را تغییر دهید', save='recive')
            return False
        _, d = read(f'{self.temp}d.wav')
        if self.ui.metadata.isChecked():
            metadata_size = self.ui.metadata_size.value()
            write('{}msw.wav'.format(self.temp), 44100, d[-metadata_size:])
            cmd = '{} -q -f {}msw.wav  --rx-one -R 44100 -M 500 -S 5000 2205'.format(
                self.minimodem, self.temp)
            ms = run(cmd, shell=True, capture_output=True).stdout
            if len(ms) != 3:
                self.alert('e', 'خطا در آنالیز متادیتا', save='recive')
                return False
            ms = int.from_bytes(ms, 'big')
        else:
            ms, ok = QtWidgets.QInputDialog.getInt(
                None, "عدد", "عدد را وارد کنید:")
            if not ok:
                return False
        # add extra class for reserve metadat
        gap = (d.size - metadata_size) // ((ms//sample_per_seg)+1)
        i = 0
        # m = int16(zeros(ms))
        m = int32([])
        # fixLastFramsize = 0
        # while i*sample_per_seg < ms:

        # pyqtRemoveInputHook()
        # breakpoint()
        while i < (ms//sample_per_seg) + 1:
            # print(i,d.size ,ms,i*sample_per_seg,(i+1)*sample_per_seg,i*gap,i*gap+sample_per_seg -fixLastFramsize)
            # if ms - (i*sample_per_seg) < sample_per_seg:
            # fixLastFramsize = sample_per_seg - (ms - i*sample_per_seg)
            if ms - m.size < sample_per_seg:
                m = append(m, d[i*gap: i*gap + (ms-m.size)])
                break
            m = append(m, d[i*gap:(i*gap)+sample_per_seg])
            # m[i*sample_per_seg:(i+1)*sample_per_seg]  = d[i*gap:i*gap+sample_per_seg -fixLastFramsize]
            i += 1
        # pyqtRemoveInputHook()
        # breakpoint()
        # print(i*gap-(i*gap+m.size-(i*sample_per_seg)))
        # print(i*sample_per_seg,ms,m.size)
        # m[i*sample_per_seg:m.size] = d[i*gap: i*gap+m.size-(i*sample_per_seg)] # last segment
        write('{}m.wav'.format(self.temp), sample_rate, m)
        cmd = '{} -f {}m.wav -q -M {} -S {} -R {}  {}'.format(
            self.minimodem, self.temp, f0, f1, sample_rate, buad_rate)
        r = run(cmd, shell=True, capture_output=True)
        if r.returncode == 0:
            m = self.gpg.dec(r.stdout)
            return m

    def changeCryptoMode(self):
        if self.ui.symmetric.isChecked():
            self.cryptoMode = 'symmetric'
            self.ui.label_11.setEnabled(False)
            self.ui.reciver.setEnabled(False)
        elif self.ui.asymmetric.isChecked():
            self.cryptoMode = 'asymmetric'
            self.ui.label_11.setEnabled(True)
            self.ui.reciver.setEnabled(True)

    def importKey(self):
        f_address, _ = getOpenFileName(
            None, " فایل را انتخاب کنید", "", "key file (*.gpg , *.pgp)")
        if not _:
            return
        self.gpg.import_key(f_address)
        self.gpg.list_key()

    def exportKey(self):
        self.gpg.export_key()

    def alert(self, type, body, title='', save=False):
        if type == 'i':
            type = QtWidgets.QMessageBox.Icon.Information
        if type == 'e':
            title = 'خطا'
            type = QtWidgets.QMessageBox.Icon.Critical
        m = QtWidgets.QMessageBox(type, title, body, parent=self.ui)
        m.show()
        if save == 'send':
            self.ui.send_status.setText(body)
        if save == 'recive':
            self.ui.recive_status.setText(body)

    def input_raw(self):
        f_address, _ = getOpenFileName(
            None, " فایل صوتی را انتخاب کنید", "", "sound file (*.flac , *.wav)")
        if not _:
            self.ui.recive_status.setText('کاربر فرایند را کنسل کرد')
            return
        self.ui.input_address.setText(f_address)
        self.recive_method = 'raw'

    def input_file(self):
        f_address, _ = getOpenFileName(
            None, " فایل را انتخاب کنید", "", "file mp3/mp4 (*.mp3 , *.mp4)")
        if not _:
            self.ui.recive_status.setText('کاربر فرایند را کنسل کرد')
            return
        self.ui.input_address.setText(f_address)
        self.recive_method = 'file'

    def video_cover(self):
        video_address, _ = getOpenFileName(
            None, " یک فایل ویديویی انتخاب کنید", "", "mp4 files (*.mp4)")
        if not _:
            self.ui.send_status.setText('کاربر فرایند را کنسل کرد')
            return
        self.ui.video_cover_address.setText(video_address)
        self.save_video_cover = True

    def save_raw(self):
        f0 = self.ui.f0.value()
        f1 = self.ui.f1.value()
        volume = self.ui.volume.value()
        buad_rate = self.ui.buad_rate.value()
        sample_rate = self.ui.sample_rate.value()
        f_address, _ = getSaveFileName(
            None, " محل ذخیره را انتخاب کنید", "", "raw (*.flac , *.wav)")
        if not _:
            self.ui.send_status.setText('کاربر فرایند را کنسل کرد')
            return
        msg = self.ui.send_msg.toPlainText()
        if not self.gpg.enc(msg):
            self.ui.send_status.setText('فقط می دونیم که خطایی رخ داده')
            return
        cmd = '{} --tx -M {} -S {} -v {} -R {} -f {} {}'.format(
            self.minimodem, f0, f1, volume, sample_rate, f_address, buad_rate)
        Popen(cmd.split(' '), stdin=open(self.temp+'m'))
        # Popen(['minimodem', '--tx', '-M','18000','-S','17000','-f', f_address , '1200'],stdin=PIPE).communicate(bytes(msg,'utf-8'))
        self.alert('i', 'save in:'+f_address, save='send')
        try:
            remove(self.temp+'m')
        except:
            pass

    def save_general_file(self):  # make mo.wav file
        self.ui.send_status.clear()
        if not self.save_video_cover:
            self.alert('e', 'ویدئوی کاور انتخاب نشده است', save='send')
            return False
        msg = self.ui.send_msg.toPlainText()
        if not self.gpg.enc(msg):
            self.ui.send_status.setText('خطا در مرحله رمزنگاری')
            return False
        f0 = self.ui.f0.value()
        f1 = self.ui.f1.value()
        buad_rate = self.ui.buad_rate.value()
        if f0 // buad_rate == f1 // buad_rate:
            self.alert(
                'e', 'با توجه به سرعت مقدار f0 و f1 خیلی به هم نزدیک اند. f0یاf1یا سرعت را تغییر دهید', save='send')
            return False
        volume = self.ui.volume.value()
        sample_rate = self.ui.sample_rate.value()
        cover_address = self.ui.video_cover_address.text()
        cmd = '{} -loglevel +fatal -stats -i {} -ac 1 -y {}cover.wav'.format(
            self.ffmpeg, cover_address, self.temp)  # extract audio from cover
        r = run(cmd, shell=True)
        if r.returncode != 0:
            self.ui.send_status.setText('خطا در استخراج صدای ویدئوی کاور')
            return False
        cmd = '{} --tx -M {} -S {} -v {} -R {} -f {} {}'.format(
            self.minimodem, f0, f1, volume, sample_rate, self.temp+'mz.wav', buad_rate)
        r = run(cmd, shell=True, stdin=open(self.temp+'m'))
        if r.returncode != 0:
            self.ui.send_status.setText('خطا در ساخت فایل سیگنال')
            return False
        if not self.powerOfWind():  # output in mo.wav
            self.ui.send_status.setText('خطادر طوفان سازی!')
            return False
        return True

    def save_mp3(self):
        if not self.save_general_file():
            return
        mp3Add, _ = getSaveFileName(
            None, " محل ذخیره فایل mp3 انتخاب کنید", "", "mp3 files (*.mp3)")
        if not _:
            self.ui.send_status.setText('کاربر فرایند را کنسل کرد')
            return
        cmd = f'{
            self.ffmpeg} -loglevel +fatal -stats -i {self.temp}mo.wav -y {mp3Add}'
        r = run(cmd, shell=True)
        if r.returncode == 0:
            self.alert('i', f'ذخیره شد!')
            self.ui.send_status.setText(f' در {mp3Add} ذخیره شد')
        else:
            self.ui.send_status.setText('خطا در ساخت mp3')

    def save_video(self):
        if not self.save_general_file():
            return
        cover = self.ui.video_cover_address.text()
        video_address, _ = getSaveFileName(
            None, " محل ذخیره فایل ویديویی انتخاب کنید", "", "mp4 files (*.mp4)")
        if not _:
            self.ui.send_status.setText('کاربر فرایند را کنسل کرد')
            return
        cmd = f'{self.ffmpeg} -loglevel +fatal -stats -i {
            self.temp}mo.wav -an -i {cover} -y {video_address}'
        r = run(cmd, shell=True)
        if r.returncode != 0:
            self.alert('e', 'خطا در ادغام فایل صدا با ویدئو', save='send')
            return
        self.ui.send_status.setText('saved in ' + video_address)
        self.alert('i', f'فایل در {video_address} دخیره شد', save='send')

    def recive_do(self):
        self.ui.recive_status.clear()
        f0 = self.ui.f0.value()
        f1 = self.ui.f1.value()
        buad_rate = self.ui.buad_rate.value()
        sample_rate = self.ui.sample_rate.value()

        if self.recive_method == 'raw':
            file_address = self.ui.input_address.text()
            cmd = '{} --rx -q -M {} -S {} -R {} --rx-one -f {} {}'.format(
                self.minimodem, f0, f1, sample_rate, file_address, buad_rate)
            r = run(cmd, shell=True, capture_output=True)
            if r.returncode == 0:
                m = self.gpg.dec(r.stdout)

        elif self.recive_method == 'file':
            file_address = self.ui.input_address.text()
            cmd = '{} -loglevel +fatal -stats -i {} -ac 1 -y {}d.wav'.format(
                self.ffmpeg, file_address, self.temp)
            r = run(cmd, shell=True)
            if r.returncode != 0:
                self.alert('e', 'خطا در تبدیل فایل مرحله ۱')
                return
            m = self.dumper()
            if not m:
                self.alert('e', 'خطای ناشناخته', save='recive')
                return
        elif self.recive_method == 'link':
            pass
        else:
            self.ui.recive_status.setText('ورودی انتخاب نشده است!')
            return

        self.alert('i', 'فایل رمزگشایی شد', save='recive')
        self.ui.recive_msg.setPlainText(m)


class GPG():
    def __init__(self):
        try:
            mkdir('k')
            # if uname().system  == 'Linux':
            # run('chmod 600 ./k',shell=True)
        except FileExistsError:
            pass
        self.gpg = gnupg.GPG(gnupghome='k', verbose=False)
        self.gpg.encoding = 'utf-8'

    def setUI(self, gui):
        self.ui = gui
        if len(self.gpg.list_keys(True)) == 0:  # first time
            name, _ = QtWidgets.QInputDialog.getText(
                None, 'ساخت کلید برای اولین بار', 'هیچ کلید خصوصی وجود ندارد برای ساخت یک کلید جدید یک نام وارد کنید')
            if not _:
                self.ui.alert('e', 'کاربر ساخت کلید را کنسل کرد')
                return
            l = string.ascii_letters + string.digits
            rand = ''.join((random.choice(l)
                           for i in range(random.randint(5, 10))))
            option = {
                'name_real': name,
                'name_comment': '',
                'key_length': 4096,
                'name_email': f'{name}@{rand}'
            }
            if uname().system != 'Linux':
                password, _ = QtWidgets.QInputDialog.getText(
                    None, "پسورد", "یک پسورد وارد کنید:", QtWidgets.QLineEdit.Password)
                if not _:
                    return
                option['passphrase'] = password
            key = self.gpg.gen_key(self.gpg.gen_key_input(**option))
            self.ui.alert('i', f'کلید{name}@{rand} ساخته شد')

        self.list_key()

    def import_key(self, filename):
        with open(filename) as f:
            key = self.gpg.import_keys(f.read())
            self.gpg.trust_keys(key.fingerprints, 'TRUST_ULTIMATE')

    def export_key(self):
        f_address, _ = getSaveFileName(
            None, " محل ذخیره را انتخاب کنید", "", "pgp (*.pgp)")
        if not _:
            return
        with open(f_address, 'w') as f:
            f.write(self.gpg.export_keys(
                self.gpg.list_keys(True)[0]['uids'][0]))

    def list_key(self):
        keys = [i['uids'][0] for i in self.gpg.list_keys()]
        ui.ui.reciver.addItems(keys)

    def enc(self, msg: str):
        cover = self.ui.temp+'cover.wav'
        if self.ui.cryptoMode == 'symmetric':
            if uname().system == 'Linux':
                msg_e = self.gpg.encrypt(
                    data=msg, recipients=None, symmetric='AES-256', armor=False, output=self.ui.temp+'m')  # debug
            else:  # on windews gpg could not get Password
                pass_phrase, ok = QtWidgets.QInputDialog.getText(
                    None, "پسورد", "پسورد را وارد کنید:", QtWidgets.QLineEdit.Password)
                if not ok:
                    return
                msg_e = self.gpg.encrypt(
                    data=msg, recipients=None, symmetric='AES256', armor=False, passphrase=pass_phrase, output=self.ui.temp+'m')
        if self.ui.cryptoMode == 'asymmetric':
            reciver = self.ui.ui.reciver.currentText()
            msg_e = self.gpg.encrypt(msg, reciver, output=self.ui.temp+'m')
        # if self.ui.ui.zip.isChecked():
            # try:
            # with (zipfile.ZipFile(ui.temp+'mz','w',compression=zipfile.ZIP_LZMA)) as z:
            # z.write(self.ui.temp+'m')
            # done = True
            # except Exception as e :
            # print(e)
            # import sys
            # print(sys.exc_info())
            # print('except')
            # done = False
            # raise
            # finally:
            # return done
        return True

    def dec(self, msg: bytes) -> str:
        # if self.ui.ui.zip.isChecked():
        # f = io.BytesIO(msg)
        # with (zipfile.ZipFile(f,compression=zipfile.ZIP_LZMA)) as z:
        # msg = z.read('m')
        if uname().system == 'Linux':
            m = self.gpg.decrypt(msg)
        else:  # only windows
            pass_phrase, ok = QtWidgets.QInputDialog.getText(
                None, "پسورد", "پسورد را وارد کنید:", QtWidgets.QLineEdit.Password)
            if not ok:
                return
            m = self.gpg.decrypt(msg, passphrase=pass_phrase)
        return str(m)


class GPG_UI():
    def __init__(self):
        self.ui = uic.loadUi('gui.ui')
        self.gpg = gnupg.GPG(gnupghome='k', verbose=False)
        self.gpg.encoding = 'utf-8'
        self.list_key()
        self.status = self.ui.status.setText
        self.ui.e.toggled.connect(self.toggle_option)  # its also apply on d
        self.ui.s.toggled.connect(self.toggle_option)  # its also apply on a
        self.ui.z.toggled.connect(self.toggle_option)
        self.ui.fi.toggled.connect(self.toggle_option)
        self.ui.fo.toggled.connect(self.toggle_option)
        self.ui.g.clicked.connect(self.gen_key)
        self.ui.i.clicked.connect(self.import_key)
        self.ui.x.clicked.connect(self.export_key)
        self.ui.convert.clicked.connect(self.convert)
        self.ui.cleari.clicked.connect(self.ui.input.clear)
        self.ui.clearo.clicked.connect(self.ui.output.clear)

    def toggle_option(self):
        e = self.ui.e.isChecked()  # encrypt
        d = self.ui.d.isChecked()  # decrypt
        z = self.ui.z.isChecked()  # zip compress
        s = self.ui.s.isChecked()  # symmetric
        a = self.ui.a.isChecked()  # asymmetric
        fi = self.ui.fi.isChecked()  # file input
        fo = self.ui.fo.isChecked()  # file output
        r = self.ui.r  # recipients
        rl = self.ui.rl  # recipients label
        ipt = self.ui.input
        opt = self.ui.output

        if s:
            r.setEnabled(False)
            rl.setEnabled(False)
        if a:
            r.setEnabled(True)
            rl.setEnabled(True)
        if fi:
            ipt.setEnabled(False)
        else:
            ipt.setEnabled(True)
        if fo:
            opt.setEnabled(False)
        else:
            opt.setEnabled(True)
        if e and z:
            opt.setEnabled(False)
            ipt.setEnabled(True)
            self.ui.fo.setChecked(True)
        if not z and not fo:
            opt.setEnabled(True)
        if d:
            self.ui.z.setEnabled(False)
            self.ui.a.setEnabled(False)
            self.ui.s.setEnabled(False)
            self.ui.r.setEnabled(False)
            self.ui.rl.setEnabled(False)

        if e:
            self.ui.z.setEnabled(True)
            self.ui.s.setEnabled(True)
            self.ui.a.setEnabled(True)

    def convert(self):
        e = self.ui.e.isChecked()  # encrypt
        d = self.ui.d.isChecked()  # decrypt
        z = self.ui.z.isChecked()  # zip compress
        s = self.ui.s.isChecked()  # symmetric
        a = self.ui.a.isChecked()  # asymmetric
        fi = self.ui.fi.isChecked()  # file input
        fo = self.ui.fo.isChecked()  # file output
        r = self.ui.r  # recipients
        ipt = self.ui.input
        opt = self.ui.output
        linux = uname().system == 'Linux'
        gpg_option = dict()
        if fi:
            fia, _ = getOpenFileName(
                None, 'فایل ورودی را انتخاب کنید', '', 'all files (*)')
            if not _:
                return
            fia = open(fia, 'rb')
            gpg_option['file'] = fia
        else:  # if not fi
            if e:
                gpg_option['data'] = ipt.toPlainText()
            else:
                gpg_option['message'] = ipt.toPlainText()

        if fo:
            foa, _ = getSaveFileName(
                None, 'فایل خروجی را مشخص کنید', '', 'all files (*)')
            if not _:
                return
            gpg_option['output'] = foa

        if not linux:
            password, _ = QtWidgets.QInputDialog.getText(
                None, "پسورد", "پسورد را وارد کنید:", QtWidgets.QLineEdit.Password)
            if not _:
                return
            gpg_option['passphrase'] = password

        if e and z:
            gpg_option['armor'] = False
        if e and s:
            if linux:
                gpg_option['symmetric'] = 'AES-256'
            else:
                gpg_option['symmetric'] = 'AES256'

            gpg_option['recipients'] = None
        if e and a:
            gpg_option['recipients'] = r.currentText()

        if e:
            if fi and fo:
                self.gpg.encrypt_file(**gpg_option)
            if fi and not fo:
                o = self.gpg.encrypt_file(**gpg_option)
                opt.setPlainText(o._as_text())
            if not fi and fo:
                self.gpg.encrypt(**gpg_option)
            if not fi and not fo:
                o = self.gpg.encrypt(**gpg_option)
                opt.setPlainText(o._as_text())
        if d:
            if fi and fo:
                self.gpg.decrypt_file(**gpg_option)
            if fi and not fo:
                o = self.gpg.decrypt_file(**gpg_option)
                opt.setPlainText(o._as_text())
            if not fi and fo:
                self.gpg.decrypt(**gpg_option)
            if not fi and not fo:
                o = self.gpg.decrypt(**gpg_option)
                opt.setPlainText(o._as_text())
        if fi:
            fia.close()

    def list_key(self):
        keys = [i['uids'][0] for i in self.gpg.list_keys()]
        self.ui.r.addItems(keys)

    def gen_key(self):
        name, _ = QtWidgets.QInputDialog.getText(
            None, 'نام', 'یک نام وارد کنید')
        if not _:
            self.status('کاربر ساخت کلید را کنسل کرد')
            return
        l = string.ascii_letters + string.digits
        rand = ''.join((random.choice(l) for i in range(10)))
        option = {
            'name_real': name,
            'name_comment': '',
            'key_length': 4096,
            'name_email': f'{name}@{rand}'
        }
        if uname().system != 'Linux':
            password, _ = QtWidgets.QInputDialog.getText(
                None, "پسورد", "پسورد را وارد کنید:", QtWidgets.QLineEdit.Password)
            if not _:
                return
            option['passphrase'] = password
        key = self.gpg.gen_key(self.gpg.gen_key_input(**option))
        self.status(f'کلید{name}@{rand} ساخته شد')

    def import_key(self):
        filename, _ = getOpenFileName(
            None, " کلید مخاطب  را انتخاب کنید", "", "pgp (*.pgp)")
        if not _:
            return
        with open(filename) as f:
            key = self.gpg.import_keys(f.read())
            self.gpg.trust_keys(key.fingerprints, 'TRUST_ULTIMATE')
        self.list_key()

    def export_key(self):
        key_list = [i['uids'][0] for i in self.gpg.list_keys(True)]
        uid, _ = QtWidgets.QInputDialog.getItem(
            None, 'لیست کلیدها', 'لطفا یک کلید/هویت را انتخاب کنید', key_list, 0, False)
        if not _:
            return
        f_address, _ = getSaveFileName(
            None, " محل ذخیره را انتخاب کنید", "", "pgp (*.pgp)")
        if not _:
            return
        with open(f_address, 'w') as f:
            f.write(self.gpg.export_keys(uid))
            ui.alert('i', 'کلید {} ذخیره شد'.format(uid))


class DUI():
    def __init__(self):
        self.ui = uic.loadUi('dui.ui')
        self.ui.e.toggled.connect(self.toggle)
        self.ui.d.clicked.connect(self.download)
        if uname().system == 'Linux':
            self.ytd = 'yt-dlp'
        else:
            self.ytd = 'ytd.exe'

    def download(self):
        f, _ = getSaveFileName(
            None, 'محل ‌ذخیره را انتخاب کنید', '', 'all (*)')
        if not _:
            return
        proxy = ''
        if self.ui.e.isChecked():
            host = self.ui.h.text()
            port = self.ui.p.text()
            proxy = f'socks5://{host}:{port}'
            link = self.ui.l.text()
        cmd = f'{self.ytd} --proxy {proxy} -o {f} {link}'
        run(cmd, shell=True)

    def toggle(self):
        if self.ui.e.isChecked():
            self.ui.h.setEnabled(True)
            self.ui.hl.setEnabled(True)
            self.ui.p.setEnabled(True)
            self.ui.pl.setEnabled(True)
        else:
            self.ui.h.setEnabled(False)
            self.ui.hl.setEnabled(False)
            self.ui.p.setEnabled(False)
            self.ui.pl.setEnabled(False)


app = QtWidgets.QApplication([])
gpg = GPG()
gpg_ui = GPG_UI()
dui = DUI()
ui = gui(gpg)
gpg.setUI(ui)
ui.ui.show()
app.exec()
