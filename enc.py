#!python3
from subprocess import run
from os.path import getsize
from scipy.io.wavfile import read
from scipy.io.wavfile import write
import argparse
parser = argparse.ArgumentParser(description="minimodem,ffmpeg")
parser.add_argument("message", help='message string')
parser.add_argument(
    "-raw", help='raw mode not encrypt message', action='store_true')
parser.add_argument("f0", help='f0', type=int)
parser.add_argument("f1", help='f1', type=int)
parser.add_argument("-b", help='buad rate default: 1225', type=int)
parser.add_argument('-s', help='sample rate(default 44100)')
parser.add_argument('-v', help='volume(default 1)', type=float)
parser.add_argument("cover", help='cover.wav', type=str)
parser.add_argument(
    "-meta", help='reserve 1000 sample from cover end for metadata(currently only msgsize)', action='store_true')
arg = parser.parse_args()

if not arg.raw:
    import gnupg
    GPG = gnupg.GPG()

f0 = arg.f0
f1 = arg.f1

sample_rate = 44100
if arg.s != None:
    sample_rate = arg.s

buad_rate = 1225  # bit per sec.
if arg.b != None:
    buad_rate = arg.b
if sample_rate % buad_rate != 0:
    print('warning: remaining sampleRate / buadRate is not zero({})'.format(sample_rate % buad_rate))

volume = 1
if arg.v != None:
    volume = arg.v

fs, cover = read(arg.cover)
cover = cover.copy()

# cover.setflags(write=1) # set writable

if not arg.raw:
    e = GPG.encrypt(data=arg.message, recipients=None, symmetric='AES-256',
                    armor=False, output='/tmp/msg.gpg', passphrase='a')
    if not e.ok:
        print('Error in encryption')
        exit()
else:
    with open('/tmp/msg.gpg', 'w') as f:
        f.write(arg.message)

cmd = 'minimodem --tx -f /tmp/msg.wav -M {} -S {} -R {} -v {} {}'.format(
    f0, f1, sample_rate, volume, buad_rate)
run(cmd, stdin=open('/tmp/msg.gpg'), shell=True)
fs, msg_compact = read('/tmp/msg.wav')
print('total msg sample', msg_compact.size)

metadata_size = 0  # metadata disable
if arg.meta:
    metadata_size = 1000
    if msg_compact.size > 16777215:  # 2^24 only 3 byte reserve for msgsize in meta sapace
        print('E: message too large for metadata in file')
        exit()
    msgsize_fix_width = f'{msg_compact.size:024b}'
    msgsize_fix_width = [msgsize_fix_width[i*8:i*8+8]
                         for i in range(3)]  # split into 3*8 slice
    msgsize_fix_width = [int(i, 2)
                         for i in msgsize_fix_width]  # convert to int
    # we must save this to file because of passin this with subprocess communicate is very slow \
    # so make a lot of useless sample
    with open('/tmp/msgsize', 'wb') as f:
        f.write(bytes(msgsize_fix_width))
    cmd = 'minimodem -t -f /tmp/msgsize.wav -R 44100 -M 500 -S 5000 2205'
    with open('/tmp/msgsize') as f:
        run(cmd, stdin=f, shell=True)
    _, msgsize_wave = read('/tmp/msgsize.wav')
    print(msgsize_wave.size)
    if msgsize_wave.size >= metadata_size:  # we only reserve 1000 sample
        print(f'msgsize could not add to metadata (too large:{
              msgsize_wave.size} sample)')
        exit()
    cover[cover.size - metadata_size: cover.size -
          metadata_size + msgsize_wave.size] = msgsize_wave
    # end metadata
sample_per_seg = sample_rate // buad_rate
gap = (cover.size - metadata_size) // (msg_compact.size//sample_per_seg)
if gap < sample_per_seg:
    print('E: cover too small (gap=%d)' % (gap))
    exit()
if gap < 1000:
    print('W: poor noise quality output: please select a longer cover')
print('gap:', gap)
i = 0
while i*sample_per_seg < msg_compact.size:
    cover[i*gap:i*gap+sample_per_seg] = msg_compact[i *
                                                    sample_per_seg:(i+1)*sample_per_seg]
    i += 1
write('/tmp/enc.wav', sample_rate, cover)
