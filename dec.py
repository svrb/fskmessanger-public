#!python3
import argparse
parser = argparse.ArgumentParser(description="minimodem,distrobitation,ffmpeg")
parser.add_argument("f0",help='f0',type=int)
parser.add_argument("-raw",help='dont use gpg',action='store_true')
parser.add_argument("-meta",help='get msgsize from metadata',action='store_true')
parser.add_argument("f1",help='f1',type=int)
parser.add_argument("-b",help='buad rate default: 1225',type=int)
parser.add_argument('-s',help='sample rate(default 44100)')
parser.add_argument("enc",help='enc.wav',type=str)
parser.add_argument("msgsize",help='msg size in sample',type=int,nargs='?',default=0)

arg = parser.parse_args()

if not arg.raw:
    import gnupg
    GPG = gnupg.GPG()
import numpy as np
from scipy.io.wavfile import write
from scipy.io.wavfile import read
from os.path import getsize
from subprocess import run

if arg.msgsize == 0 and not arg.meta:
    print('need msgsize or -meta')
    exit()


f0 = arg.f0
f1 = arg.f1

sample_rate = 44100
if arg.s != None:
    sample_rate = arg.s

buad_rate = 1225 #bit per sec.
if arg.b != None:
    buad_rate = arg.b
if sample_rate % buad_rate != 0:
    print('warning: remaining sampleRate / buadRate is not zero({})'.format(sample_rate % buad_rate))

fs , enc = read(arg.enc)
metadata_size = 0
if arg.meta:
    metadata_size = 1000
    wave = enc[ -metadata_size:]
    write('/tmp/msgsize.wav',44100,wave)
    cmd = 'minimodem -q -f /tmp/msgsize.wav -R 44100 -M 500 -S 5000 2205'
    msgsize_raw = run(cmd,shell=True,capture_output=True).stdout
    if len(msgsize_raw) != 3:
        print(f'E: msgsize has {len(msgsize_raw)}')
        exit()
    arg.msgsize = int.from_bytes(msgsize_raw,'big')
    del(msgsize_raw)
    del(wave)
sample_per_seg = sample_rate // buad_rate
gap = (enc.size - metadata_size) // (arg.msgsize//sample_per_seg)
i=0
msg = np.int16(np.zeros(arg.msgsize))
while i*sample_per_seg < arg.msgsize:
    msg[i*sample_per_seg:(i+1)*sample_per_seg]  = enc[i*gap:i*gap+sample_per_seg]
    i+=1

write('/tmp/recover.wav',sample_rate,msg)
cmd = 'minimodem -f /tmp/recover.wav -q -M {} -S {} -R {}  {}'.format(f0,f1,sample_rate,buad_rate)
dec = run(cmd,shell=True,capture_output=True)
if arg.raw :
    print (dec.stdout)
else:
    m = GPG.decrypt(message=dec.stdout,passphrase='a')
    print(m)

